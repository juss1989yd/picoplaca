package com.ec.picoplaca.jyandun.test;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

import com.ec.picoplaca.jyandun.PicoPlacaVerify;
import com.ec.picoplaca.jyandun.Placa;

public class PicoPlacaVerifyTest {
	
	PicoPlacaVerify picoPlacaVerify;
	Placa placa;
	
	@Before
	public void before() {
		picoPlacaVerify = new PicoPlacaVerify();
		placa = new Placa();
	}

	@Test
	public void testLastNumber() {
		int answer = picoPlacaVerify.lastNumber(7588);
		int expected = 8;
		assertEquals(answer, expected);
	}
	
	@Test
	public void testHour() {
		Boolean answer = picoPlacaVerify.hourVerify(12);
		Boolean expected = true;
		assertEquals(answer, expected);
	}
	
	@Test
	public void testVerifyDay() {
		String answer = picoPlacaVerify.verifyDay("03-06-2019");
		String expected = "Monday";
		assertEquals(answer, expected);
	}
	
	@Test
	public void testPicoPlaca() {
		placa.setNumeroPlaca(7588);
		placa.setFecha("03-06-2019");
		placa.setHora(12);
		picoPlacaVerify.picoPlaca(placa);
	}

}
