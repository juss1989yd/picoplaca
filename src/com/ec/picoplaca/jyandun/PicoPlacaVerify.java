package com.ec.picoplaca.jyandun;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PicoPlacaVerify {
	
	private static final float HOUR_START_AM = 7f;
	
	private static final float HOUR_END_AM = 9f;
	
	private static final float HOUR_START_PM = 16f;
	
	private static final float HOUR_END_PM = 19.30f;
	
	public int lastNumber(int placaIn) {
		String placa = new Integer(placaIn).toString();
		if(placa.length() > 2 && placa.length() < 5)
			return Integer.parseInt(placa.substring(placa.length() -1));
		else
			throw new IllegalArgumentException("Valor ingresado no es correcto");
	}
	
	public Boolean hourVerify(float hour) {
		if((hour >= HOUR_START_AM && hour <= HOUR_END_AM) || (hour >= HOUR_START_PM && hour <= HOUR_END_PM))
			return false;
		else
			return true;
	}
	
	public String verifyDay(String dateIn) {
		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			Date date = sdf.parse(dateIn);
			Calendar myDate = Calendar.getInstance();
			myDate.setTime(date);
			int dow = myDate.get (Calendar.DAY_OF_WEEK);
			if(dow == myDate.MONDAY)
				return "Monday";
			else if(dow == myDate.TUESDAY)
				return "Tuesday";
			else if(dow == myDate.WEDNESDAY)
				return "Wednesday";
			else if(dow == myDate.THURSDAY)
				return "Thursday";
			else if(dow == myDate.FRIDAY)
				return "Friday";
			else if(dow == myDate.SATURDAY)
				return "Saturday";
			else
				return "Sunday";
			
		}  catch(ParseException ex) {
			throw new IllegalArgumentException("Fecha ingresada es incorrecta");
		}
		catch (Exception e) {
			// TODO: handle exception
			throw new IllegalArgumentException("Error");
		}
	}
	
	public void picoPlaca(Placa placa) {
		int lastNumber = lastNumber(placa.getNumeroPlaca());
		
		if(lastNumber >= 0 && lastNumber <= 1) {
			if(verifyDay(placa.getFecha()) == "Monday") {
				if(!hourVerify(placa.getHora())) {
					System.out.println("No puede movilizarse");
				} else {
					System.out.println("En la hora indicada puede movilizarse");
				}
			} else {
				System.out.println("Puede movilizarse");
			}
		} 
		else if(lastNumber >= 2 && lastNumber <= 3) {
			if(verifyDay(placa.getFecha()) == "Tuesday") {
				if(!hourVerify(placa.getHora())) {
					System.out.println("No puede movilizarse");
				} else {
					System.out.println("En la hora indicada puede movilizarse");
				}
			} else {
				System.out.println("Puede movilizarse");
			}
		}
		else if(lastNumber >= 4 && lastNumber <= 5) {
			if(verifyDay(placa.getFecha()) == "Wednesday") {
				if(!hourVerify(placa.getHora())) {
					System.out.println("No puede movilizarse");
				} else {
					System.out.println("En la hora indicada puede movilizarse");
				}
			} else {
				System.out.println("Puede movilizarse");
			}
		}
		else if(lastNumber >= 6 && lastNumber <= 7) {
			if(verifyDay(placa.getFecha()) == "Thursday") {
				if(!hourVerify(placa.getHora())) {
					System.out.println("No puede movilizarse");
				} else {
					System.out.println("En la hora indicada puede movilizarse");
				}
			} else {
				System.out.println("Puede movilizarse");
			}
		}
		else if(lastNumber >= 8 && lastNumber <= 9) {
			if(verifyDay(placa.getFecha()) == "Friday") {
				if(!hourVerify(placa.getHora())) {
					System.out.println("No puede movilizarse");
				} else {
					System.out.println("En la hora indicada puede movilizarse");
				}
			} else {
				System.out.println("Puede movilizarse");
			}
		}
	}
	
}
