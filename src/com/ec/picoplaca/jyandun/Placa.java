package com.ec.picoplaca.jyandun;

public class Placa {
	
	private int numeroPlaca;
	
	private String fecha;
	
	private int hora;

	public int getNumeroPlaca() {
		return numeroPlaca;
	}

	public void setNumeroPlaca(int numeroPlaca) {
		this.numeroPlaca = numeroPlaca;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getHora() {
		return hora;
	}

	public void setHora(int hora) {
		this.hora = hora;
	}
	
	

}
